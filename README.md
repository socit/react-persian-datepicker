### This Package and it's Document will be more helpful in near future. (Developing...)
<br />



# React Persian Datepicker 

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/socit/react-persian-datepicker/blob/master/LICENSE)
[![npm version](https://img.shields.io/npm/v/@socit/react-persian-datepicker)](https://www.npmjs.com/package/@socit/react-persian-datepicker)

Persian-Datepicker is a library for pick date from jalali calendar.

## Installation
```
$ npm install --save @socit/react-persian-datepicker
```

## Basic Usage
```jsx
import React from 'react';
import ReactPersianDatepicker from '@socit/react-persian-datepicker';

import './App.css';

function App() {
  return (
    <div className="App">
      <ReactPersianDatepicker
        onChange={(date) => { console.log(date); }}
      />
    </div>
  );
}

export default App;
```

## Types
Currently we have two types of picker. <br />

Standalone
![@socit/react-persian-datepicker](https://i.ibb.co/Tw8N984/standalone.png ":size=300")

and, <br />

input
![@socit/react-persian-datepicker](https://i.ibb.co/7gC56jK/input.png ":size=300")

### Documentation

For documentation see [socit.gitlab.io/react-persian-datepicker](https://socit.gitlab.io/react-persian-datepicker).

### License

React-Persian-Datepicker is under the terms of [MIT licensed](https://gitlab.com/socit/react-persian-datepicker/blob/master/License).