import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PersianDate from 'persian-date';

import Core from '../core';

import './index.scss';

const className = 'react-persian-datepicker-input';

class Input extends Component {
    constructor(props) {
        super(props);

        if (props.picker && props.picker.gregorian) {
            PersianDate.toCalendar('gregorian');
            PersianDate.toLocale('en');
        }

        this.state = {
            value: '',
            pickerStatus: 0 // 0: close, 1: open
        };

        this.refInput = React.createRef();
    }

    render() {
        return (
            <div className={className} style={this.props.style}>
                <input
                    ref={this.refInput}
                    onFocus={() => {
                        this.setState({ pickerStatus: 1 });
                    }}
                    onBlur={() => {
                        this.setState(state => {
                            if (state.pickerStatus === 1) {
                                let valueArray = state.value.split('/').map(item => item.trim());
                                if (valueArray.length === 3) {
                                    if (
                                        parseInt(valueArray[0]) &&
                                        parseInt(valueArray[1]) &&
                                        parseInt(valueArray[2])
                                    ) {
                                        valueArray = valueArray.map(item => parseInt(item));
                                        const newDate = new PersianDate(valueArray);
    
                                        if (this.props.picker && this.props.picker.startDate) {
                                            const startDate = new PersianDate(this.props.picker.startDate);
                                            if (newDate.diff(startDate) < 0) {
                                                return { pickerStatus: 0, value: `${this.props.value[0]} / ${this.props.value[1]} / ${this.props.value[2]}` };
                                            }
                                        }
                                        if (this.props.picker && this.props.picker.endDate) {
                                            const endDate = new PersianDate(this.props.picker.endDate);
                                            if (newDate.diff(endDate) > 0) {
                                                return { pickerStatus: 0, value: `${this.props.value[0]} / ${this.props.value[1]} / ${this.props.value[2]}` };
                                            }
                                        }
    
                                        // if everythings OK
                                        this.props.onChange([newDate.year(), newDate.month(), newDate.date()]);
                                        return { pickerStatus: 0, value: `${newDate.year()} / ${newDate.month()} / ${newDate.date()}` };
                                    }
                                }
    
                                let value = '';
                                if (this.props.value && this.props.value[0] && this.props.value[1] && this.props.value[2]) {
                                    value = `${this.props.value[0]} / ${this.props.value[1]} / ${this.props.value[2]}`;
                                }
                                return { pickerStatus: 0, value };
                            } else {
                                return {};
                            }
                        });
                    }}
                    onChange={e => {
                        this.setState({ value: e.target.value });
                    }}
                    value={this.state.value}
                />

                <div
                    onMouseDown={e => { e.preventDefault(); }} // prevent blur input
                >
                    { this.state.pickerStatus === 1 ?
                        <Core
                            small
                            selectedDate={this.props.value}
                            onChange={date => {
                                this.setState({
                                    pickerStatus: 0,
                                    value: `${date[0]} / ${date[1]} / ${date[2]}`
                                }, () => {
                                    this.refInput.current.blur();
                                    this.props.onChange(date);
                                });
                            }}
                            style={{ position: 'absolute', top: '38px', left: '0px', borderRadius: '8px' }}

                            {...this.props.picker}
                        />
                        :
                        null
                    }
                </div>
            </div>
        );
    }
}

Input.propTypes = {
    style    : PropTypes.object,
    value    : PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
    onChange : PropTypes.func,
    picker   : PropTypes.object
};

export default Input;
