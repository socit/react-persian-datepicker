import React from 'react';
import { shallow } from 'enzyme';
import ReactPersianDatepicker from '../';

it('renders an `.react-persian-datepicker`', () => {
    const wrapper = shallow(<ReactPersianDatepicker />);
    expect(wrapper.find('.react-persian-datepicker')).toHaveLength(2);
});
