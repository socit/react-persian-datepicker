import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PersianDate from 'persian-date';

import {
    Main,
    MonthPicker,
    YearPicker
} from './screens';

import './index.scss';

const className = 'react-persian-datepicker';

class ReactPersianDatepicker extends Component {
    constructor(props) {
        super(props);

        if (props.gregorian) {
            PersianDate.toCalendar('gregorian');
            PersianDate.toLocale('en');
        }

        let startDate = null;
        if (props.startDate) {
            if (Array.isArray(props.startDate)) {
                startDate = props.startDate;
            } else {
                const newDate = new PersianDate(props.startDate).startOf('day');
                startDate = [newDate.year(), newDate.month(), newDate.date()];
            }
        }

        let endDate = null;
        if (props.endDate) {
            if (Array.isArray(props.endDate)) {
                endDate = props.endDate;
            } else {
                const newDate = new PersianDate(props.endDate).startOf('day');
                endDate = [newDate.year(), newDate.month(), newDate.date()];
            }
        }

        let selectedDate = null;
        // { from, to } format
        if (props.pickDuration) {
            if (props.selectedDate) {
                let from = null;
                let to = null;
                // set from
                if (props.selectedDate.from) {
                    if (Array.isArray(props.selectedDate.from)) {
                        from = props.selectedDate.from;
                    } else {
                        const newDate = new PersianDate(props.selectedDate.from).startOf('day');
                        from = [newDate.year(), newDate.month(), newDate.date()];
                    }
                }
                // set to
                if (props.selectedDate.to) {
                    if (Array.isArray(props.selectedDate.to)) {
                        to = props.selectedDate.to;
                    } else {
                        const newDate = new PersianDate(props.selectedDate.to).startOf('day');
                        to = [newDate.year(), newDate.month(), newDate.date()];
                    }
                }
                selectedDate = { from, to };
            } else {
                selectedDate = { from: null, to: null };
            }
        // [] format
        } else {
            if (props.selectedDate) {
                if (Array.isArray(props.selectedDate)) {
                    selectedDate = props.selectedDate;
                } else {
                    const newDate = new PersianDate(props.selectedDate).startOf('day');
                    selectedDate = [newDate.year(), newDate.month(), newDate.date()];
                }
            } else {
                const newDate = new PersianDate().startOf('day');
                selectedDate = [newDate.year(), newDate.month(), newDate.date()];
            }
        }

        let showDate = null;
        if (props.pickDuration) {
            if (selectedDate.from) {
                showDate = [selectedDate.from[0], selectedDate.from[1], selectedDate.from[2]];
            } else {
                const newDate = new PersianDate().startOf('day');
                showDate = [newDate.year(), newDate.month(), newDate.date()];
            }
        } else {
            showDate = [selectedDate[0], selectedDate[1], selectedDate[2]];
        }

        this.state = {
            startDate,
            endDate,
            selectedDate,
            showDate,

            showStatus: 0, // 0: main, 1: month picker, 2: year picker
        };

        console.log(this.state)
    }

    render() {
        return (
            <div
                className={`${className} ${this.props.small ? 'small' : ''}`}
                style={{ backgroundColor: this.props.primaryColor, ...this.props.style }}
            >
                { this.state.showStatus === 0 ?
                    <Main
                        small={this.props.small}
                        gregorian={this.props.gregorian}
                        
                        startDate={this.state.startDate}
                        endDate={this.state.endDate}
                        selectedDate={this.state.selectedDate}
                        onSelectedDateChange={newDate => {
                            this.setState({ selectedDate: newDate });
                            if (this.props.onChange) {
                                this.props.onChange(newDate);
                            }
                        }}
                        showDate={this.state.showDate}
                        onShowDateChange={newDate => {
                            this.setState({ showDate: newDate });
                        }}

                        // acceptButton={this.props.acceptButton}
                        // rejectButton={this.props.rejectButton}
                        monthNavigators={this.props.monthNavigators}

                        onShowStatusChange={status => {
                            this.setState({ showStatus: status });
                        }}

                        className={className}
                        primaryColor={this.props.primaryColor}
                        secondaryColor={this.props.secondaryColor}

                        // onAccept={this.props.onAccept}
                        // onReject={this.props.onReject}
                        onChange={this.props.onChange}
                    />
                    :
                    null
                }

                { this.state.showStatus === 1 ?
                    <MonthPicker
                        small={this.props.small}
                        gregorian={this.props.gregorian}
                        
                        showDate={this.state.showDate}
                        onShowDateChange={newDate => {
                            this.setState({ showDate: newDate, showStatus: 0 });
                        }}
                        className={className}
                        primaryColor={this.props.primaryColor}
                        secondaryColor={this.props.secondaryColor}
                    />
                    :
                    null
                }

                { this.state.showStatus === 2 ?
                    <YearPicker
                        small={this.props.small}
                        gregorian={this.props.gregorian}
                        
                        showDate={this.state.showDate}
                        onShowDateChange={newDate => {
                            this.setState({ showDate: newDate, showStatus: 0 });
                        }}
                        className={className}
                        primaryColor={this.props.primaryColor}
                        secondaryColor={this.props.secondaryColor}
                    />
                    :
                    null
                }
            </div>
        );
    }
}

ReactPersianDatepicker.propTypes = {
    small     : PropTypes.bool,
    gregorian : PropTypes.bool,

    pickDuration : PropTypes.bool,

    startDate    : PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
    endDate      : PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
    selectedDate : PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
    // showDate     : PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),

    // acceptButton    : PropTypes.object,
    // rejectButton    : PropTypes.object,
    monthNavigators : PropTypes.object,

    primaryColor   : PropTypes.string,
    secondaryColor : PropTypes.string,
    style          : PropTypes.object,

    // onAccept : PropTypes.func,
    // onReject : PropTypes.func,
    onChange : PropTypes.func,
};

ReactPersianDatepicker.defaultProps = {
    gregorian : false,

    primaryColor   : 'rgb(252, 74, 96)',
    secondaryColor : 'rgb(255, 255, 255)'
};

export default ReactPersianDatepicker;
