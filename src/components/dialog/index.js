import React from 'react';
import Core from '../core';

import './index.scss';

const className = 'react-persian-datepicker-dialog';

function Dialog(props) {
    return (
        <div className={className}>
            <Core
                style={{ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, margin: 'auto', borderRadius: '8px' }}
                {...props}
            />
        </div>
    );
}

export default Dialog;
