import core from './components/core';
import dialog from './components/dialog';
import input from './components/input';

export {
    dialog as ReactPersianDatepickerDialog,
    input as ReactPersianDatepickerInput
};
export default core;
