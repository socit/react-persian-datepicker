### style
You can control style of input with `style` prop.
```jsx
<ReactPersianDatepickerInput
  style={{ position: 'absolute', top: 0, right: 0, bottom: '100px', left: 0, margin: 'auto' }}
/>
```

### Value
You can control selectedDate with `value` prop.
```jsx
<ReactPersianDatepickerInput
  value={[1398, 6, 6]}
/>
```

### On Change
This function calls when selectedDate changed. It will receive new selectedDate as an argument. <br />
This is write on top of Standalone component onChange function.
```jsx
<ReactPersianDatepickerInput
  onChange={(selectedDate) => { console.log(selectedDate); }}
/>
```

### Picker
This prop pass directly into built-in standalone component.
```jsx
<ReactPersianDatepickerInput
  picker={{
    primaryColor: 'rgb(0, 0, 0)'
  }}
/>
```
> NOTE: We recommend not to use `selectedDate`, `style` and `onChange` props in here. These props used by input component.
