# Introduction

> An awesome project.

## Quick Start

First need to install package. <br />
Open a terminal at project directory and type this command:
```
$ npm install --save @socit/react-persian-datepicker
```

Now you can import package any where you want on project:
```jsx
import React from 'react';
import ReactPersianDatepicker from '@socit/react-persian-datepicker';

import './App.css';

function App() {
  return (
    <div className="App">
      <ReactPersianDatepicker
        onChange={(date) => { console.log(date); }}
      />
    </div>
  );
}

export default App;
```

That's it. <br />

## Why ?
With react-persian-datepicker you have access to standalone picker component. This helps you shape picker with your project design principles. For example you can create your own dialog and add this datepicker to it OR you can just show datepicker on main page OR anything else you want. <br />
Because some developer prefer plug-and-play components, we decide create some wrapper components around this datepicker. you can find them in this document. <br />
There is Another reason we think this package will be useful for you. react-persian-datepicker created to be fully customizable, and you can pass many props to reach the standards you expect from datepicker.